{ lib
, buildNpmPackage
, fetchFromGitHub
, nodejs_20
}:

buildNpmPackage rec {
  pname = "homebridge-z2m";
  version = "1.11.0-beta.2";
  src = fetchFromGitHub {
    owner = "itavero";
    repo = "homebridge-z2m";
    rev = "v${version}";
    hash = "sha256-bIgJXkg683FTW2tGe9wnoldiQMxIIZ1LLYavx5/nrSo=";
  };

  nodejs = nodejs_20;

  npmDepsHash = "sha256-kpCDpY2PZGiNJEEP9RDbqDlIgRMkznri7DzY/UOtXIY=";
}
