let
    sources = import npins/default.nix;
    nixpkgs-2311 = sources."nixos-23.11".outPath;
    nixpkgs-2405-small = sources."nixos-24.05-small".outPath;
    nixpkgs-2405 = sources."nixos-24.05".outPath;
    nixpkgs-2411 = sources."nixos-24.11".outPath;
in rec 
{
    mile-end = (import "${nixpkgs-2311}/nixos" {
        configuration = ./mile-end/configuration.nix;
        system = "x86_64-linux";
    }).system;

    shenfield-nix = (import "${nixpkgs-2411}/nixos" {
        configuration = ./shenfield-nix/configuration.nix;
        system = "x86_64-linux";
    }).system;

    hammersmith = (import "${nixpkgs-2411}/nixos" {
        configuration = ./hammersmith/configuration.nix;
        system = "x86_64-linux";
    }).system;

    holborn = (import "${nixpkgs-2311}/nixos" {
        configuration = ./holborn/configuration.nix;
        system = "x86_64-linux";
    }).system;

    stratford = (import "${nixpkgs-2311}/nixos" {
        configuration = ./stratford/configuration.nix;
        system = "x86_64-linux";
    }).system;

    euston = (import "${nixpkgs-2411}/nixos" {
        configuration = ./euston/configuration.nix;
        system = "x86_64-linux";
    }).system;
}
