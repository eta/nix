#!/usr/bin/env bash

set -euo pipefail

function die {
	echo "$1" 1>&2;
	exit 1
}


[ $# -eq 3 ] || die "usage: $0 [netns] [ipv4] [ipv6]"

NAME=$1
ADDR_V4=$2
ADDR_V6=$3

HASH=$(printf '%s' "$NAME" | sha1sum -) # done this way to appease shellcheck SC2059
VETH_NAME="veth${HASH:0:6}"
GATEWAY_LLADDR="fe80:1312:${HASH:0:4}:${HASH:4:4}::1"

[ -f "/var/run/netns/$NAME" ] && die "fatal: netns '$NAME' already exists"

set -x

ip netns add "$NAME"

ip link add "$VETH_NAME" type veth peer "in-$VETH_NAME"
ip link property add dev "$VETH_NAME" altname "veth-$NAME"
ip link set "in-$VETH_NAME" netns "$NAME"

ip -n "$NAME" link set "in-$VETH_NAME" name "eth0"

ip -n "$NAME" link set lo up
ip -n "$NAME" link set eth0 up

ip -n "$NAME" addr add "$ADDR_V4/32" dev eth0
ip -n "$NAME" route add 0/0 nexthop via inet6 "$GATEWAY_LLADDR" dev eth0

ip -n "$NAME" -6 addr add "$ADDR_V6/128" dev eth0
ip -n "$NAME" -6 route add default via "$GATEWAY_LLADDR" dev eth0

ip link set "$VETH_NAME" up

sleep 1

# FIXME(eta): If you don't do this the veth might just stay in
#                 State: degraded (pending)
#             for whatever fucking reason??
udevadm trigger
