{ config, lib, pkgs, ... }:

 let
    netns-up = pkgs.writeShellApplication {
        name = "netns-up";
        runtimeInputs = with pkgs; [ iproute2 coreutils-full ];
        text = builtins.readFile ./netns-up.sh;
    };
    sha1Of = (veth: builtins.hashString "sha1" veth);
    hashVeth = (veth: builtins.substring 0 6 (sha1Of veth));
    underscorify = (name: builtins.replaceStrings ["-"] ["_"] name);
    namespaces = config.networking.namespaces;
in
{
    options = {
        networking.namespaces = with lib; mkOption {
            type = types.attrsOf (types.submodule {
                options = {
                    ipv4 = mkOption {
                        type = types.str;
                    };
                    ipv6 = mkOption {
                        type = types.str;
                    };
                    services = mkOption {
                        type = types.listOf types.str;
                        default = [];
                    };
                };
            });
            default = {};
        };
    };
    config.systemd.services = lib.concatMapAttrs (name: options: {
        "netns-${name}" = {
            description = "${name} network namespace";
            wantedBy = [ "multi-user.target" ];
            serviceConfig = {
                Type = "oneshot";
                RemainAfterExit = true;
                ExecStart = "${netns-up}/bin/netns-up ${name} ${options.ipv4} ${options.ipv6}";
                # ExecStopPost runs in both normal stop and failure scenarios, which is what we want (to clean up
                # the partially created netns)
                ExecStopPost = [
                    "-${pkgs.iproute2}/bin/ip link del veth${hashVeth name}"
                    "-${pkgs.iproute2}/bin/ip netns del ${name}"
                ];
                Restart = "on-failure";
                RestartSec = 1;
            };
        };
    } // lib.attrsets.mergeAttrsList (map (service: {
        "${service}" = {
            bindsTo = lib.mkAfter ["netns-${name}.service"];
            after = lib.mkAfter ["netns-${name}.service"];
            serviceConfig.NetworkNamespacePath = "/run/netns/${name}";
        };
    }) options.services)) namespaces;
    config.systemd.network.networks = lib.concatMapAttrs (name: options: {
        "90-netns-${name}".extraConfig = ''
            [Match]
            Name=veth${hashVeth name}

            [Network]
            Address=fe80:1312:${builtins.substring 0 4 (sha1Of name)}:${builtins.substring 4 4 (sha1Of name)}::1/128
            RequiredForOnline=no
        '';
    }) namespaces;
    config.services.bird2.config = lib.mkAfter (builtins.concatStringsSep "\n" (builtins.attrValues (builtins.mapAttrs
            (name: options: ''
                protocol static netns4_${underscorify name} {
                    ipv4 {
                        import filter send_to_ospf;
                    };
                    route ${options.ipv4}/32 via "veth${hashVeth name}";
                }
                protocol static netns6_${underscorify name} {
                    ipv6 {
                        import filter send_to_ospf;
                    };
                    route ${options.ipv6}/128 via "veth${hashVeth name}";
                }
            '') namespaces)));
}
