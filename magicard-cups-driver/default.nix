# SPDX-FileCopyrightText: 2024 Luke Granger-Brown <depot@lukegb.com>
#
# SPDX-License-Identifier: Apache-2.0

{ stdenv
, lib
, fetchurl
, fetchFromGitHub
, cmake
, cups
}:

stdenv.mkDerivation rec {
  pname = "magicard-cups-driver";
  version = "1.4.0";

  src = fetchurl {
    url = "https://f08ddbe93aa02eaf9a6c-f08cd513e3a8c914f4f8f62af1786149.ssl.cf3.rackcdn.com/magicard_ltd-linux_driver-1.4.0.tar.gz";
    hash = "sha256-OAdpxnGMqVcf3bEtYQ9PACZFJvq8gzhGxuBBnliwoso=";
  };

  postPatch = ''
    cp ${./CMakeLists.txt} ./CMakeLists.txt

      substituteInPlace ppd/*.ppd \
      --replace-fail 'rastertoultra' "$out/lib/cups/filter/rastertoultra" \
      --replace-fail 'cmdtoultra' "$out/lib/cups/filter/cmdtoultra"
  '';

  cmakeFlags = [
    "-DCUPS_DATA_DIR=share/cups"
    "-DCUPS_SERVER_BIN=lib/cups"
  ];

  nativeBuildInputs = [ cmake cups ];
}
