{ config, lib, pkgs, ... }:

{
  imports = [
  ];

  networking.domain = "i.eta.st";
  networking.useDHCP = false;
  networking.firewall.enable = false;
  systemd.network.enable = true;
  
  nix.settings.trusted-users = [ "eta" "root" ];
  nixpkgs.config.allowUnfree = true;
  nix.package = pkgs.lix;
  nix.settings.extra-experimental-features = ["nix-command"];

  i18n.defaultLocale = "en_GB.UTF-8";
  time.timeZone = "Europe/London";

  documentation.man.generateCaches = false; # takes ages, rebuilds all the time

  boot.kernel.sysctl = {
    "net.ipv4.ip_forward" = 1;
    "net.ipv6.conf.all.forwarding" = 1;
  };

  users.users.eta = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keyFiles = [ ./eta-keys.txt ];
    shell = pkgs.fish;
  };

  environment.systemPackages = lib.mkBefore (with pkgs; [ vim curl htop tcpdump dig tmux sqlite-interactive pv nmap git nftables ipcalc ]);
  programs.fish.enable = true;
  programs.mtr.enable = true;
  programs.vim.defaultEditor = true;

  services.openssh.enable = true;
  services.openssh.settings.PasswordAuthentication = false;
  services.openssh.settings.KbdInteractiveAuthentication = false;
  environment.etc.ssh-ca-key.source = ./ca-key.txt;
  services.openssh.settings.TrustedUserCAKeys = "/etc/ssh-ca-key";

  # Make systemd restart things forever every second;
  # see https://michael.stapelberg.ch/posts/2024-01-17-systemd-indefinite-service-restarts
  environment.etc."systemd/system.conf.d/infinite-restarts.conf".text = ''
  [Manager]
  DefaultRestartSec=1s
  DefaultStartLimitIntervalSec=0
  '';
}
