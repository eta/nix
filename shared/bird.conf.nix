{ ipv4, ipv6, ospfInterfaces, extraConfig ? "", kernelTable ? null }:

''
router id ${ipv4};

ipv4 table master4;
ipv6 table master6;

log syslog { info, remote, warning, error, auth, fatal, bug, debug, trace };
debug protocols off;

attribute int to_ospf;
attribute int to_peering;

filter send_to_ospf {
        to_ospf = 1;
        accept;
}

protocol device {
}

protocol direct {
        ipv4 {
                import filter send_to_ospf;
        };
        ipv6 {
                import filter send_to_ospf;
        };
}

protocol kernel kernel4 {
        '' + (if kernelTable != null then "kernel table ${kernelTable};" else "") + ''
        ipv4 {
                export filter {
                        krt_prefsrc = ${ipv4};
                        accept;
                };
        };
}

protocol kernel kernel6 {
        '' + (if kernelTable != null then "kernel table ${kernelTable};" else "") + ''
        ipv6 {
                export filter {
                        krt_prefsrc = ${ipv6};
                        accept;
                };
        };
}

protocol ospf v2 etatriangle {
        ipv4 {
                import all;
                export where to_ospf = 1 || source = RTS_OSPF;
        };

        area 0 {
'' +
builtins.concatStringsSep "\n" (map (interface: ''
                interface "${interface}" {
			hello 3;
                        bfd on;
                };
'') ospfInterfaces)
+ ''
        };
}


protocol ospf v3 etasix {
        ipv6 {
                import all;
                export where to_ospf = 1 || source = RTS_OSPF;
        };

        area 0 {
'' +
builtins.concatStringsSep "\n" (map (interface: ''
                interface "${interface}" {
			hello 3;
                        bfd on;
                };
'') ospfInterfaces)
+ ''
        };
}

protocol bfd {
        debug { states, events, routes };
'' +
builtins.concatStringsSep "\n" (map (interface: ''
        interface "${interface}" {
                interval 200 ms;
                multiplier 5;
        };
'') ospfInterfaces)
+ ''
}
'' + extraConfig
