#!/usr/bin/env bash

set -euo pipefail

ATTR=euston

echo "==> Getting nix-output-monitor"
NOM=$(nix-build -E --no-out-link 'with import <nixpkgs> {}; nix-output-monitor')

echo "==> Building attribute $ATTR"
$NOM/bin/nom-build -A "$ATTR" -j 8

RESULT=$(readlink -f ./result)

#sudo "$RESULT/bin/switch-to-configuration" dry-activate
#read -p "==> Deploy $RESULT (Y/N)? " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
echo "==> Updating system profile"
sudo nix-env -p /nix/var/nix/profiles/system --set "$RESULT"
echo "==> Switching to new configuration"
sudo "$RESULT/bin/switch-to-configuration" switch

