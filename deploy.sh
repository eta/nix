#!/usr/bin/env bash

set -euo pipefail

if [[ ! $# -eq 2 ]]; then
    echo "usage: $0 attribute destination"
    exit 1
fi

ATTR=$1
DEST=$2

echo "==> Getting nix-output-monitor"
NOM=$(nix-build -E -o "./out/nom" 'with import <nixpkgs> {}; nix-output-monitor')

echo "==> Building attribute $ATTR"
$NOM/bin/nom-build -A "$ATTR" -j 8 -o "./out/$ATTR"

RESULT=$(readlink -f "./out/$ATTR")

echo "==> Copying $RESULT to $DEST"
nix copy --substitute-on-destination --no-check-sigs --to "ssh-ng://$DEST" "$RESULT"
echo "==> Starting deployment on $DEST"
ssh -t "$DEST" bash -c "'
set -euo pipefail
sudo \"$RESULT/bin/switch-to-configuration\" dry-activate
read -p \"==> Deploy $RESULT (Y/N)? \" confirm && [[ \$confirm == [yY] || \$confirm == [yY][eE][sS] ]] || exit 1
echo \"==> Updating system profile\"
sudo nix-env -p /nix/var/nix/profiles/system --set \"$RESULT\"
echo \"==> Switching to new configuration\"
sudo \"$RESULT/bin/switch-to-configuration\" switch
'"
