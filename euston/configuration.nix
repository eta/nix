# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports =
    [
      ../shared/generic-server.nix
      ./hardware-configuration.nix
    ];
  
  # Overriding stuff from generic-server.nix that we don't need
  # because a laptop is not a server:
  systemd.network.enable = lib.mkForce false;
  services.openssh.enable = lib.mkForce false;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 0;
  boot.loader.efi.canTouchEfiVariables = true;
  # boot.plymouth.enable = true;
  # Use systemd in stage 1.
  boot.initrd.systemd.enable = true;

  networking.hostName = "euston"; # Define your hostname.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "Europe/London";

  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true; # use xkb.options in tty.
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  # Configure keymap in X11
  services.xserver.xkb.layout = "gb";
  services.xserver.xkbOptions = "ctrl:swapcaps";
  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  services.yubikey-agent.enable = true;
  services.pcscd.enable = true;

  services.fwupd.enable = true;

  environment.systemPackages = lib.mkBefore (with pkgs; [
    firefox
    google-chrome
    age
    passage
    age-plugin-yubikey
    nix-output-monitor
    npins
    gnome.gnome-tweaks
    rustup
    lm_sensors
    discord
    vscode
    emacs
    sbcl
    networkmanagerapplet # for nm-connection-editor
    gnome.gnome-power-manager # for gnome power statistics
    gnome.eog
    gcc
    ripgrep
    python3
    gedit
    python311Packages.pip
    gajim
    usbutils
    kicad-small
    vlc
    libimobiledevice
    ifuse
    jetbrains.rust-rover
    ncdu
    virt-manager
    qalculate-gtk
    gimp
    inkscape
    bundler
    wireguard-tools
    pkg-config
    openssl.dev
    stdenv
    direnv
    jq
    whois
  ]);

  services.lorri.enable = true;
  services.usbmuxd.enable = true;
  systemd.oomd.enableRootSlice = true;
  systemd.oomd.enableUserServices = true;

  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-color-emoji
    fantasque-sans-mono
    inter
  ];

  programs.fish.enable = true;
  programs.mtr.enable = true;
  programs.steam.enable = true;
  
  # Enable CUPS to print documents.
  services.printing.enable = true;

  services.printing.drivers = [
    (pkgs.callPackage ../magicard-cups-driver {})
  ];

  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    audio.enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };
  hardware.pulseaudio.enable = false;


  services.logind = {
    lidSwitch = "suspend-then-hibernate";
    extraConfig = ''
      HandlePowerKey=suspend-then-hibernate
    '';
  };

  systemd.sleep.extraConfig = "HibernateDelaySec=30m";

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

