{ pkgs, lib, ... }:

let
  ipam = import ../ipam.nix;
in
{
  services.keycloak = {
    enable = true;
    settings = {
      http-port = 8080;
      hostname = "keycloak.i.eta.st";
      http-enabled = true;
      proxy-headers = "xforwarded";
      http-host = "127.0.0.1";
      # this horrifying monstrosity is via https://jdbc.postgresql.org/documentation/use/#unix-sockets;
      # see also https://kohlschutter.github.io/junixsocket/dependency.html
      db-url = lib.mkForce "jdbc:postgresql://localhost/keycloak?socketFactory=org.newsclub.net.unix.AFUNIXSocketFactory$FactoryArg&socketFactoryArg=/run/postgresql/.s.PGSQL.5432";
    };
    package = pkgs.keycloak.override {
      # bundle in the junixsocket stuff into the keycloak build so that the above hack actually works
      plugins = [(builtins.fetchTarball {
        url = "https://github.com/kohlschutter/junixsocket/releases/download/junixsocket-2.9.0/junixsocket-dist-2.9.0-bin.tar.gz";
        sha256 = "00dsd6606c35rq21qv0ab2q8d6akh8njc3kcqb4bivbjj3f14651";
      } + "/lib/")];
    };
    # the module doesn't provide a way to opt out of this :(
    database.passwordFile = "/etc/blank";
  };
  environment.etc.blank.text = "";

  services.multicaddy.keycloak = {
    config = ''
      keycloak.i.eta.st

      redir / /realms/etainfra/account

      @unauthorized {
            not path /js/*
            not path /realms/*
            not path /resources/*
            not path /robots.txt
            not remote_ip 127.0.0.0/16 44.31.189.0/24 2a0d:1a40:7553::/48
      }

      respond @unauthorized "Access denied" 403 {
            close
      }

      reverse_proxy localhost:8080 {
            header_up -Forwarded
      }
    '';
  };

  networking.namespaces.keycloak = {
    ipv4 = ipam.ipv4.keycloak;
    ipv6 = ipam.ipv6.keycloak;
    services = ["keycloak" "caddy-keycloak"];
  };
}
