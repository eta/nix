{ pkgs, lib, config, ... }:

let
  ipam = import ../ipam.nix;
  sources = import ../npins/default.nix;
  paroxysm-ng = pkgs.callPackage sources.paroxysm-ng.outPath {};
in
{
  services.ergochat = {
    enable = true;
    settings = lib.mkForce (import ./ergo-config.nix {
      sslCrt = "/var/lib/acme/pandemonium.i.eta.st/fullchain.pem";
      sslKey = "/var/lib/acme/pandemonium.i.eta.st/key.pem";
    });
  };
  systemd.services.ergochat = {
    # Don't have the default restart-on-every-change behaviour, please
    restartTriggers = lib.mkForce [];
    reloadTriggers = lib.mkForce [ config.services.ergochat.configFile ];
    serviceConfig.EnvironmentFile = "/var/lib/ergo-env";
    serviceConfig.Group = "acme";
  };
  systemd.services.paroxysm-ng = {
    description = "paroxysm-ng quote database";
    after = [ "network.target" "network-online.target" "ergochat.service" ];
    requires = [ "network-online.target" ];
    wantedBy = [ "multi-user.target" ];
    bindsTo = [ "ergochat.service" ];

    serviceConfig = {
      Type = "simple";
      DynamicUser = true;

      ExecStart = "${paroxysm-ng}/bin/paroxysm-ng /var/lib/paroxysm-ng/config.ini";

      StateDirectory = [ "paroxysm-ng" ];
      NoNewPrivileges = true;
      PrivateDevices = true;
      ProtectHome = true;
      TimeoutStopSec = "5s";
      PrivateTmp = true;
      ProtectSystem = "full";
    };
  };
  networking.namespaces.pandemonium = {
    ipv4 = ipam.ipv4.pandemonium;
    ipv6 = ipam.ipv6.pandemonium;
    services = ["ergochat" "acme-pandemonium.i.eta.st" "paroxysm-ng"];
  };
}
