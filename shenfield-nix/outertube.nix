{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
  sources = import ../npins/default.nix;
  outertube = (pkgs.callPackage "${sources.outertube.outPath}/Cargo.nix" {}).rootCrate.build;
in
{
  services.multicaddy.outertube = {
    config = ''
    outertube.i.eta.st

    reverse_proxy localhost:8000
    '';
  };

  systemd.services.outertube = {
    description = "outertube";
    wantedBy = [ "multi-user.target" ];

    serviceConfig = {
      ExecStart = "${outertube}/bin/outertube /var/lib/outertube/config.toml";
      DynamicUser = true;
      StateDirectory = "outertube";
      Restart = "on-failure";
      RestartSec = 3;
    };
  };

  networking.namespaces.outertube = {
    ipv4 = ipam.ipv4.outertube;
    ipv6 = ipam.ipv6.outertube;
    services = ["outertube" "caddy-outertube"];
  };

}
