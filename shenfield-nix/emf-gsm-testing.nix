{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
  bonzi-laptop-v4 = "45.151.214.244";
  bonzi-endpoint = "45.151.214.244:5666";
  emfcore-v4 = "90.155.21.62";
  emfcore-endpoint = "90.155.21.62:6061";
in
{
  services.asterisk = {
    enable = true;
    confFiles."extensions.conf" = ''
[emfcore_in]
exten = _X.,1,Dial(PJSIP/''${EXTEN}@osmocom)
same = n,Hangup()

[osmocom_in]
exten = 100,1,Answer()                                                                                                                                        
same = n,Wait(1)                                                                                                                                              
same = n,Playback(hello-world)                                                 
same = n,Hangup()

exten = 200,1,Answer()               
same = n,Set(TIMEOUT(absolute)=60)                                             
same = n,Playback(demo-echotest)       
same = n,Echo()   

exten = _X.,1,Dial(PJSIP/''${EXTEN}@emfcore)
same = n,Hangup()
    '';
    extraConfig = ''
[options]
verbose = 3
    '';
    confFiles."logger.conf" = ''
[general]
[logfiles]
console => notice,warning,error
syslog.local0 => notice,warning,error,debug,verbose(3)
    '';
    confFiles."pjsip.conf" = ''
;; These things are just templates. You should skip past this section.
[endpoint-template](!)
type=endpoint
transport=default-transport
allow=all
direct_media=no
inband_progress=yes
context=internal

[auth-template](!)
type=auth
auth_type=userpass

[aor-template](!)
type=aor
max_contacts=1
remove_existing=yes
qualify_frequency=30

[default-transport]
type=transport
protocol=udp
bind=0.0.0.0:5060

;; incoming calls from osmocom

[osmocom](endpoint-template)
trust_id_inbound=yes
aors=osmocom
allow=!all,gsm
context=osmocom_in

[osmocom]
type = aor
contact = sip:${bonzi-endpoint}
qualify_frequency=30

[osmocom]
type = identify
endpoint = osmocom
match = ${bonzi-endpoint}

;; EMF core

[emfcore](endpoint-template)
trust_id_inbound=yes
aors=emfcore
allow=!all,g722,ulaw
context=emfcore_in

[emfcore]
type = aor
contact = sip:${emfcore-endpoint}
qualify_frequency=30

[emfcore]
type = identify
endpoint = emfcore
match = ${emfcore-endpoint}
    '';
  };
  systemd.services.emf-gsm-nftables = let
    nftables-conf = pkgs.writeTextDir "nftables.conf" ''
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
        chain input {
                type filter hook input priority 0;
                ct state vmap { invalid : drop, established : accept, related : accept }
                iif "lo" accept
                ip protocol icmp accept
                ip6 nexthdr ipv6-icmp accept
                ip saddr ${bonzi-laptop-v4} meta l4proto {tcp, udp} counter accept comment "SIP inbound"
                ip saddr ${emfcore-v4} meta l4proto {tcp, udp} counter accept comment "SIP inbound"
                reject;
        }
        chain forward {
                type filter hook forward priority 0;
        }
        chain output {
                type filter hook output priority 0;
        }
}
    '';
  in {
    description = "emf-gsm nftables config";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.nftables}/bin/nft -f ${nftables-conf}/nftables.conf";
      RemainAfterExit = true;
      Type = "oneshot";
    };
  };

  environment.systemPackages = lib.mkAfter (with pkgs; [
    sngrep
    nftables
  ]);

  networking.namespaces.emf-gsm-testing = {
    ipv4 = ipam.ipv4.emf-gsm-testing;
    ipv6 = ipam.ipv6.emf-gsm-testing;
    services = ["asterisk" "emf-gsm-nftables"];
  };
}