{ config, lib, pkgs, ... }:

let
    cfg = config.services.multicaddy;
in
{
    options.services.multicaddy = with lib; mkOption {
        type = types.attrsOf (types.submodule {
            options = {
                package = mkPackageOption pkgs "caddy" {};
                config = mkOption {
                    type = types.lines;
                };
                group = mkOption {
                    type = types.nullOr types.str;
                    default = null;
                };
                oauth2 = mkOption {
                    type = types.nullOr (types.submodule {
                        options = {
                            package = mkPackageOption pkgs "oauth2-proxy" {};
                            config = mkOption {
                                type = types.lines;
                            };
                            environmentFile = mkOption {
                                type = types.path;
                            };
                        };
                    });
                    default = null;
                };
            };
        });
        default = {};
    };
    config.systemd.services = lib.concatMapAttrs (name: options: let
        Caddyfile = pkgs.writeTextDir "Caddyfile" options.config;
        Caddyfile-formatted = pkgs.runCommand "Caddyfile-formatted-${name}" { nativeBuildInputs = [ options.package ]; } ''
          mkdir -p $out
          cp --no-preserve=mode ${Caddyfile}/Caddyfile $out/Caddyfile
          caddy fmt --overwrite $out/Caddyfile
        '';
    in {
        "caddy-${name}" = {
            description = "Caddy for ${name}";
            after = [ "network.target" "network-online.target" ];
            requires = [ "network-online.target" ];
            wantedBy = [ "multi-user.target" ];
            reloadTriggers = [ Caddyfile-formatted ];

            serviceConfig = {
                Type = "notify";
                DynamicUser = true;

                ExecStart = "${options.package}/bin/caddy run --config ${Caddyfile-formatted}/Caddyfile";
                ExecReload = "${options.package}/bin/caddy reload --config ${Caddyfile-formatted}/Caddyfile --force";

                StateDirectory = [ "caddy-${name}" ];
                LogsDirectory = [ "caddy-${name}" ];
                Environment = [ "XDG_DATA_HOME=/var/lib/caddy-${name}" ];

                Restart = "on-failure";
                RestartPreventExitStatus = 1;
                RestartSec = "5s";

                NoNewPrivileges = true;
                PrivateDevices = true;
                ProtectHome = true;
                TimeoutStopSec = "5s";
                LimitNOFILE = 1048576;
                LimitNPROC = 512;
                PrivateTmp = true;
                AmbientCapabilities = "CAP_NET_ADMIN CAP_NET_BIND_SERVICE";
                ProtectSystem = "full";
            } // (if options.group != null then { Group = options.group; } else {});
        };
    } // (if options.oauth2 == null then {} else let
        configFile = pkgs.writeTextDir "oauth2-proxy.cfg" options.oauth2.config;
    in {
        "oauth2-proxy-${name}" = {
            description = "oauth2-proxy for ${name}";
            after = [ "network.target" "network-online.target" ];
            requires = [ "network-online.target" ];
            wantedBy = [ "multi-user.target" ];
            reloadTriggers = [ configFile ];

            serviceConfig = {
                Type = "simple";
                DynamicUser = true;

                ExecStart = "${options.oauth2.package}/bin/oauth2-proxy --config=${configFile}/oauth2-proxy.cfg";
                ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";

                EnvironmentFile = [ options.oauth2.environmentFile ];

                Restart = "on-failure";
                RestartSec = "3s";

                NoNewPrivileges = true;
                PrivateDevices = true;
                ProtectHome = true;
                TimeoutStopSec = "5s";
                LimitNOFILE = 1048576;
                LimitNPROC = 512;
                PrivateTmp = true;
                ProtectSystem = "full";
            };
        };
    })) cfg;
}