{ pkgs, lib, ... }:

let
  ipam = import ../ipam.nix;
in
{
  services.ejabberd = {
    enable = true;
    package = pkgs.ejabberd.override {
      withSqlite = true;
    };
    configFile = pkgs.writeText "ejabberd.yml" (
      import ./ejabberd.yml.nix {
        ipv4 = ipam.ipv4.ejabberd;
        ipv6 = ipam.ipv6.ejabberd;
        sslCrt = "/var/lib/acme/eta.st/fullchain.pem";
        sslKey = "/var/lib/acme/eta.st/key.pem";
      }
    );
  };
  systemd.services.ejabberd.serviceConfig.Environment = lib.mkAfter ["ERL_EPMD_ADDRESS=127.0.0.1"];

  users.users.ejabberd.extraGroups = [ "acme" ];

  networking.namespaces.ejabberd = {
    ipv4 = ipam.ipv4.ejabberd;
    ipv6 = ipam.ipv6.ejabberd;
    services = ["ejabberd"];
  };

}