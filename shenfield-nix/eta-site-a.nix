{ pkgs, ... }:

let
  sources = import ../npins/default.nix;
  ipam = import ../ipam.nix;
  eta-website = pkgs.callPackage sources.eta-website.outPath {};
  linx-server = pkgs.callPackage ./linx-server.nix {};
  linx-server-config = pkgs.writeText "config.ini" ''
    filespath = /var/lib/linx-server/files/
    metapath = /var/lib/linx-server/meta/
    realip = true
    siteurl = https://eta.st/lx/
    sitename = eta.st/lx
    bind = [::1]:8080
  '';
in
{
    systemd.services.linx-server = {
        description = "linx-server";
        after = [ "network.target" "network-online.target" ];
        requires = [ "network-online.target" ];
        wantedBy = [ "multi-user.target" ];
        restartTriggers = [ linx-server-config ];

        serviceConfig = {
            Type = "simple";
            DynamicUser = true;

            ExecStart = "${linx-server}/bin/linx-server -config ${linx-server-config}";

            StateDirectory = [ "linx-server" ];
            NoNewPrivileges = true;
            PrivateDevices = true;
            ProtectHome = true;
            TimeoutStopSec = "5s";
            LimitNOFILE = 1048576;
            LimitNPROC = 512;
            PrivateTmp = true;
            ProtectSystem = "full";
        };
    };

    networking.namespaces.eta-site-a = {
        ipv4 = ipam.ipv4.eta-site-a;
        ipv6 = ipam.ipv6.eta-site-a;
        services = ["linx-server" "caddy-eta-site-a"];
    };

    services.multicaddy.eta-site-a = {
        config = ''
        eta.st {
            root * ${eta-website}

            log {
                output file /var/log/caddy-eta-site-a/access.log {
                    roll_disabled
                }
                format filter {
                    wrap json
                    fields {
                        common_log delete
                        resp_headers delete
                        request>tls delete
                    }
                }
            }

            @unauthorized {
                not remote_ip 44.31.189.0/24 2a0d:1a40:7553::/48 127.0.0.0/16
            }
            @notupload {
                path /lx/upload
                not remote_ip 44.31.189.0/24 10.0.0.0/8 49.12.129.211/32 185.230.221.0/24 2a0d:1a40:7553::/48 2a0c:2f06::/32 2a01:4f8:242:5b21:0:feed:edef:beef/128 2a01:4f8:212:1c59:ffff:ffff:0:4/128
            }
            handle /lx/* {
                respond @notupload 403 {
                    close
                }
                reverse_proxy localhost:8080
            }
            handle /.well-known/matrix/client {
                header {
                    Access-Control-Allow-Origin *
                }
		
            }
	    handle_path /.well-known/matrix/* {
	        header Access-Control-Allow-Origin *
	        header Content-Type application/json

	        respond /client `{ "m.homeserver": { "base_url": "https://state-reset.i.eta.st" } }`
	        respond /server `{ "m.server": "state-reset.i.eta.st:443" }`
	        respond 404
	    }
            handle /.well-known/webfinger {
                reverse_proxy https://gotosocial.i.eta.st
            }
            handle /.well-known/host-meta {
                redir https://gotosocial.i.eta.st/.well-known/host-meta permanent
            }
            handle /.well-known/nodeinfo {
                redir https://gotosocial.i.eta.st/.well-known/nodeinfo permanent
            }
            handle {
                file_server
                encode gzip
                try_files {path} {path}/ {path}.html =404
            }
            handle_errors {
                rewrite * /{http.error.status_code}.html
                file_server
            }
        }

        theta.eu.org, www.theta.eu.org {
            redir https://eta.st{uri} permanent
        }
        '';
    };
}
