{ pkgs, lib, ... }:

let
  ipam = import ../ipam.nix;
  sources = import ../npins/default.nix;
  gts-config = import ./gts-config.nix;
  bp = pkgs.callPackage sources.nix-npm-buildpackage {};
  gts-web-dst = bp.buildYarnPackage {
    src = sources.gotosocial.outPath + "/web/source";
    preInstallPhases = [];
    yarnBuild = ''
yarn install
yarn ts-patch install
yarn build
    '';
    installPhase = ''
mkdir -p $out/
cp -r ../assets $out/
    '';
  };
  gts-web = pkgs.runCommand "gotosocial-web" {} ''
    mkdir $out
    cp -rv "${sources.gotosocial.outPath}/web/assets" $out/
    cp -rv "${sources.gotosocial.outPath}/web/template" $out/

    chmod +w $out/assets/
    cp -rv "${gts-web-dst}/assets/" $out/
    chmod -w $out/assets/

    chmod -R +w $out/template/
    ${pkgs.patch}/bin/patch -p0 -d $out/ < ${./gts-imessage-embed.patch}
    chmod -R -w $out/template/
  '';
  gts-custom = pkgs.gotosocial.overrideAttrs {
    version = "git-${builtins.substring 0 8 sources.gotosocial.revision}";
    src = sources.gotosocial.outPath;
    doCheck = false;
  };
in
{
  services.gotosocial.enable = true;
  services.gotosocial.package = gts-custom;
  services.gotosocial.settings = gts-config // {
    web-asset-base-dir = "${gts-web}/assets/";
    web-template-base-dir = "${gts-web}/template/";
  };
  services.gotosocial.environmentFile = "/var/lib/gotosocial/env";
  networking.namespaces.gotosocial = {
    ipv4 = ipam.ipv4.gotosocial;
    ipv6 = ipam.ipv6.gotosocial;
    services = ["gotosocial"];
  };
}
