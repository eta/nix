{ lib, ... }:

let
  ipam = import ../ipam.nix;
in
{
  services.step-ca = {
    enable = true;
    settings = builtins.fromJSON (builtins.readFile ./step-ca.json);
    address = "[::]";
    port = 443;
    intermediatePasswordFile = "/var/lib/step-ca/secrets/password-file";
  };
  systemd.services.step-ca.serviceConfig.StartLimitBurst = lib.mkForce 60;
  networking.namespaces.step = {
    ipv4 = ipam.ipv4.step;
    ipv6 = ipam.ipv6.step;
    services = ["step-ca"];
  };
}
