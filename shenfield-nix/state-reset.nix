{ ... }:

let
  ipam = import ../ipam.nix;
in
{
  services.matrix-synapse = {
    enable = true;
    extras = ["systemd" "user-search" "oidc"];
    extraConfigFiles = [
      "/var/lib/matrix-synapse/oidc.yaml" # Put the OIDC provider config in here with the secret
    ];
    log.root.level = "WARNING"; # hush
    settings = rec {
      server_name = "eta.st";
      public_baseurl = "https://state-reset.i.eta.st/"; # haha, get it
      password_config.enabled = false;
      limit_remote_rooms = {
        enabled = true;
        complexity = 3;
        complexity_error = "This room is too complicated, and would probably cause my disks to melt.";
      };
      database = {
        name = "sqlite3";
        args = {
          database = "/var/lib/matrix-synapse/homeserver.db";
          cp_max = 8;
        };
      };
      media_store_path = "/var/lib/matrix-synapse/media";
      signing_key_path = "/var/lib/matrix-synapse/homeserver.signing.key";
      settings.listeners = [{
        port = 8008;
        bind_addresses = [ "::1" ];
        type = "http";
        tls = false;
        x_forwarded = true;
        resources = [ {
          names = [ "client" "federation" ];
          compress = true;
        } ];
      }];
    };
  };
  services.multicaddy.state-reset = {
    config = ''
    state-reset.i.eta.st {
      reverse_proxy localhost:8008
    }
    '';
  };
  networking.namespaces.state-reset = {
    ipv4 = ipam.ipv4.state-reset;
    ipv6 = ipam.ipv6.state-reset;
    services = ["caddy-state-reset" "matrix-synapse"];
  };
}