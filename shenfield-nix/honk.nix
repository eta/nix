{ config, lib, ... }:

let
  ipam = import ../ipam.nix;
in
{
  services.honk = {
    enable = true;
    host = "[::1]";
    username = "eta";
    servername = "h.eta.st";
  };
  # Disable the honk initdb thing the nixos module tries to do
  systemd.services.honk.bindsTo = lib.mkForce ["netns-honk.service"];
  systemd.services.honk-initdb = lib.mkForce {};
  # The default preStart tries to run honk backup before honk upgrade, which fails if the db actually needs an upgrade...
  systemd.services.honk.preStart = let cfg = config.services.honk; in lib.mkForce ''
    mkdir -p $STATE_DIRECTORY/views
    ${lib.getExe cfg.package} -datadir $STATE_DIRECTORY -viewdir ${cfg.package}/share/honk upgrade
    ${lib.getExe cfg.package} -datadir $STATE_DIRECTORY -viewdir ${cfg.package}/share/honk cleanup
  '';
  services.multicaddy.honk = {
    config = ''
    h.eta.st

    handle {
            header X-Robots-Tag "noindex"
            reverse_proxy localhost:5757
    }
    '';
  };
  networking.namespaces.honk = {
    ipv4 = ipam.ipv4.honk;
    ipv6 = ipam.ipv6.honk;
    services = ["caddy-honk" "honk"];
  };
}
