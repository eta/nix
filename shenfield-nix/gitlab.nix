{ config, ... }:

let
  ipam = import ../ipam.nix;
in
{
  services.gitlab = {
    enable = true;
    https = true;
    host = "git.eta.st";
    port = 443;
    smtp = {
      enable = true;
      address = "mail.i.eta.st";
      port = 25;
      domain = "mail.i.eta.st";
      enableStartTLSAuto = false;
      opensslVerifyMode = "none";
    };
    initialRootPasswordFile = "/var/gitlab/state/secrets/irp-secret";
    secrets = {
      dbFile = "/var/gitlab/state/secrets/db-secret";
      secretFile = "/var/gitlab/state/secrets/secret-secret";
      otpFile = "/var/gitlab/state/secrets/otp-secret";
      jwsFile = "/var/gitlab/state/secrets/jws-secret";
    };
    extraShellConfig = {
      user = "git";
      sshd = {
        listen = "[::]:22";
        host_key_files = [
          "/var/gitlab/state/secrets/ssh_host_rsa_key"
          "/var/gitlab/state/secrets/ssh_host_ecdsa_key"
          "/var/gitlab/state/secrets/ssh_host_ed25519_key"
        ];
      };
    };
    extraConfig = {
      email_from = "gitlab@mail.i.eta.st";
      email_reply_to = "noreply@mail.i.eta.st";
      default_theme = 3;
      default_projects_features = {
        issues = false;
        merge_requests = false;
        wiki = false;
        snippets = false;
        builds = false;
        container_registry = false;
      };
      omniauth = {
        enabled = true;
        allow_single_sign_on = ["openid_connect"];
        sync_profile_from_provider = ["openid_connect"];
        sync_profile_attributes = ["email" "name"];
        block_auto_created_users = false;
        providers = [{
          name = "openid_connect";
          label = "Keycloak";
          args = {
            name = "openid_connect";
            scope = ["openid" "profile" "email"];
            response_type = "code";
            issuer = "https://keycloak.i.eta.st/realms/etainfra";
            client_auth_method = "query";
            discovery = true;
            uid_field = "preferred_username";
            pkce = true;
            client_options = {
              identifier = "gitlab";
              secret = { _secret = "/var/gitlab/state/secrets/oidc-secret"; };
              redirect_uri = "https://git.eta.st/users/auth/openid_connect/callback";
            };
          };
        }];
      };
    };
    registry.enable = false;
    pages.enable = false;
  };

  systemd.services.gitlab-sshd = {
    after = [ "network.target" "gitlab.service" ];
    bindsTo = [ "gitlab.service" ];
    wantedBy = [ "gitlab.target" ];
    partOf = [ "gitlab.target" ];

    serviceConfig = {
      Type = "simple";
      User = config.systemd.services.gitlab.serviceConfig.User;
      Group = config.systemd.services.gitlab.serviceConfig.Group;
      Restart = "on-failure";
      WorkingDirectory = config.systemd.services.gitlab.serviceConfig.WorkingDirectory;
      ExecStart = "${config.services.gitlab.packages.gitlab-shell}/bin/gitlab-sshd -config-dir /run/gitlab/";
      AmbientCapabilities = "CAP_NET_BIND_SERVICE";
    };
  };

  services.multicaddy.gitlab = {
    config = ''
    git.eta.st

    reverse_proxy unix//run/gitlab/gitlab-workhorse.socket
    '';
  };

  networking.namespaces.gitlab = {
    ipv4 = ipam.ipv4.gitlab;
    ipv6 = ipam.ipv6.gitlab;
    services = ["gitlab-postgresql" "gitlab-config" "gitlab-db-config" "gitlab-sidekiq" "redis-gitlab"
                "gitaly" "gitlab-workhorse" "gitlab" "gitlab-backup" "caddy-gitlab" "gitlab-sshd"];
  };

}