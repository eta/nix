{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
  doorbell = pkgs.callPackage ./doorbell-pkg.nix {};
in
{
  systemd.services.doorbell = {
    description = "etaflat doorbell access controller";
    wantedBy = [ "multi-user.target" ];

    serviceConfig = {
      ExecStart = "${doorbell}/bin/doorbell /var/lib/doorbell/config.toml";
      DynamicUser = true;
      StateDirectory = "doorbell";
      Restart = "on-failure";
      RestartSec = 3;
    };
  };
  services.multicaddy.doorbell = {
    config = ''
    doorbell.i.eta.st

    reverse_proxy localhost:8080
    '';
  };
  networking.namespaces.doorbell = {
    ipv4 = ipam.ipv4.doorbell;
    ipv6 = ipam.ipv6.doorbell;
    services = ["caddy-doorbell" "doorbell"];
  };
}