{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
in
{
    users.users.wiki-data = {
        isSystemUser = true;
        createHome = true;
        home = "/var/lib/wiki-data/";
        homeMode = "770"; # allows caddy running as wiki-data group to access, too
        group = "wiki-data";
    };
    users.groups.wiki-data = {};
    
    services.phpfpm.pools.wiki = {
        user = "wiki-data";
        settings = {
            "listen.owner" = "wiki-data";
            "pm" = "dynamic";
            "pm.max_children" = 32;
            "pm.max_requests" = 500;
            "pm.start_servers" = 2;
            "pm.min_spare_servers" = 2;
            "pm.max_spare_servers" = 5;
            "php_admin_value[error_log]" = "stderr";
            "php_admin_flag[log_errors]" = true;
            "catch_workers_output" = true;
        };
        phpEnv = {
            "IMAGEMAGICK_CONVERT" = "${pkgs.imagemagick}/bin/convert";
            "DIFF3" = "${pkgs.diffutils}/bin/diff3";
            "GS" = "${pkgs.ghostscript}/bin/gs";
            "PDFINFO" = "${pkgs.poppler_utils}/bin/pdfinfo";
            "PDFTOTEXT" = "${pkgs.poppler_utils}/bin/pdftotext";
        };
    };

    networking.namespaces.wiki = {
        ipv4 = ipam.ipv4.wiki;
        ipv6 = ipam.ipv6.wiki;
        services = ["phpfpm-wiki" "caddy-wiki" "oauth2-proxy-wiki"];
    };

    services.multicaddy.wiki = {
        config = ''
            wiki.i.eta.st

            reverse_proxy /oauth2/* localhost:4180

            @nonauth {
                    not path /oauth2/*
                    not {
                            path /links.txt
                            remote_ip 2a0d:1a40:7553:feed:216:abff:fe31:c478
                    }
            }
            forward_auth @nonauth localhost:4180 {
                    uri /
                    copy_headers {
                            X-Auth-Request-Preferred-Username
                    }
            }
            
            log stdout
            encode gzip
            rewrite /w/* /index.php?title={path}
            rewrite /rest.php/* /rest.php?{query}
            redir / /w/Main_Page

            root * /var/lib/wiki-data/webroot/
            file_server
            php_fastcgi unix/${config.services.phpfpm.pools.wiki.socket}
        '';
        group = "wiki-data";
        oauth2 = {
            config = ''
                http_address = "127.0.0.1:4180"
                reverse_proxy = true
                upstreams = ["static://202"]
                provider = "oidc"
                client_id = "wiki"
                oidc_issuer_url = "https://keycloak.i.eta.st/realms/etainfra"
                cookie_refresh = "30m"
                allowed_groups = ["etawiki"]
                email_domains = "*"
                code_challenge_method = "S256"
                provider_display_name = "Keycloak"
                set_xauthrequest = true
            '';
            environmentFile = "/var/lib/oauth2-wiki-env";
        };
    };
}
