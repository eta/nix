# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
  sources = import ../npins/default.nix;
in
{
  imports =
    [ 
      ../shared/generic-server.nix
      ./hardware-configuration.nix
      ../netns/module.nix
      ./multicaddy.nix
      ./eta-site-a.nix
      ./gitlab.nix
      ./state-reset.nix
      ./gotosocial.nix
      ./step.nix
      ./ejabberd.nix
      ./pandemonium.nix
      ./keycloak.nix
      ./intertube.nix
      ./honk.nix
      # ./doorbell.nix (don't live here any more!)
      ./wiki.nix
      # ./emf-gsm-testing.nix
      ./clickhouse.nix
      ./pdns.nix
      ./outertube.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "shenfield-nix";
  systemd.network.networks."10-uplink".extraConfig = ''
[Match]
Name=enp1s0

[Network]
IPv6AcceptRA=yes
Address=${ipam.ipv4.shenfield-nix}/30
Gateway=${ipam.ipv4.shnix0-shenfield}
DNS=1.1.1.1
  ''; 

  networking.wireguard.interfaces = {
    etavpn = {
      ips = [ "${ipam.ipv4.vpn-server}/29" "${ipam.ipv6.vpn-server}/64" ];
      listenPort = 51820;
      privateKeyFile = "/var/lib/wgkey-etavpn";
      allowedIPsAsRoutes = true;

      peers = [ {
        publicKey = "IggvRRiYLMpRtqeJtDzpYsb8fC8sCD+uKbALHrmwngY=";
        allowedIPs = [ "${ipam.ipv4.vpn-euston}/32" "${ipam.ipv6.vpn-euston}/128" ];
      } {
        publicKey = "8+dMIy2alRP30A8GzFhTtn2PNCSWw7EtGYWMU2F4gxY=";
        allowedIPs = [ "${ipam.ipv4.vpn-der-gerat}/32" "${ipam.ipv6.vpn-der-gerat}/128" ];
      }];
    };
  };

  environment.systemPackages = with pkgs; [
    pdns
  ];

  environment.etc.nixos.source = import ../self-referential.nix;

  services.bird2 = {
    enable = true;
    config = import ../shared/bird.conf.nix {
      ipv4 = ipam.ipv4.shenfield-nix;
      ipv6 = ipam.ipv6.shenfield-nix;
      ospfInterfaces = ["enp1s0"];
    };
  };

  services.resolved.enable = true;
  environment.etc."resolv.conf" = lib.mkForce { text = "nameserver 1.1.1.1"; }; # HACK(eta): fix

  services.caddy.enable = true;
  services.caddy.extraConfig = ''
comfybed.zone {
  root * /etc/www/comfybed.zone/
  file_server
}
cutegirl.zone {
  header X-Squish "squish"
  redir https://muffinti.me/ 307
}
'';
  environment.etc."www/comfybed.zone".source = ../www/comfybed.zone;

  services.restic.backups = {
    shenfield-nix-backup = {
      environmentFile = "/var/lib/restic-env";
      passwordFile = "/var/lib/restic-pwd";
      repository = "rest:http://hammersmith.i.eta.st:8000/";
      timerConfig = {
        OnCalendar = "*-*-* 03:00:00"; # NOTE must be after postgresqlBackup
        RandomizedDelaySec = "1h";
      };
      extraBackupArgs = [
        "--verbose"
      ];
      paths = [
        "/home"
        "/root"
        "/etc/group"
        "/etc/machine-id"
        "/etc/passwd"
        "/etc/subgid"
        "/var/lib"
        "/var/backup"
        "/var/gitlab"
        "/clickhouse/var/lib/clickhouse"
      ];
      # chatops moment
      # FIXME(eta): have actual prometheus for this
      backupCleanupCommand = ''
      echo "💾 shenfield-nix backup done!" | ${pkgs.curl}/bin/curl -d @- http://metrics.i.eta.st:6001/send
      '';
      backupPrepareCommand = ''
      set -euo pipefail

      clickhouse_prepare () {
        ${pkgs.iproute2}/bin/ip netns exec clickhouse ${pkgs.clickhouse}/bin/clickhouse-client --password=intertube --query="ALTER TABLE trains UNFREEZE WITH NAME 'nightly';" || true
        ${pkgs.iproute2}/bin/ip netns exec clickhouse ${pkgs.clickhouse}/bin/clickhouse-client --password=intertube --query="ALTER TABLE trains FREEZE WITH NAME 'nightly';"
        echo "💾 clickhouse tables frozen" | ${pkgs.curl}/bin/curl -d @- http://metrics.i.eta.st:6001/send
      }
      systemctl is-active --quiet clickhouse && clickhouse_prepare
      '';
    };
  };
  # Ensure the restic backup waits for the postgres one to finish
  systemd.services.restic-backups-shenfield-nix-backup.after = lib.mkAfter ["postgresqlBackup.service"];

  networking.namespaces.test = {
    ipv4 = "44.31.189.22";
    ipv6 = "2a0d:1a40:7553:3142::1";
    services = ["caddy-test"];
  };
  services.multicaddy.test = {
    config = ''
      :80 {
        respond "it works!"
      }
    '';
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = "nixos-acme@eta.st";
    # We need this for XMPP, because of how it works
    certs."eta.st" = {
      domain = "*.eta.st";
      extraDomainNames = [ "eta.st" ];
      dnsProvider = "cloudflare";
      credentialsFile = "/var/lib/cloudflare-dns";
      reloadServices = ["ejabberd"];
    };
    certs."pandemonium.i.eta.st" = {
      listenHTTP = ":80";
      reloadServices = ["ergochat"];
    };
  };

  services.collectd = {
    enable = true;
    buildMinimalPackage = true;
    plugins = {
      cpu = "";
      df = "";
      disk = "";
      interface = "";
      load = "";
      memory = "";
      network = "Server \"metrics.i.eta.st\"";
      processes = "";
      swap = "";
      users = "";
    };
  };

  services.postgresql = {
    enable = true;
    enableTCPIP = false;
    ensureDatabases = [ "keycloak" ];
    ensureUsers = [{
      name = "keycloak";
      ensureDBOwnership = true;
    }];
  };

  services.postgresqlBackup = {
    enable = true;
    startAt = "*-*-* 01:00:00"; # NOTE must be before restic backup
    compression = "none"; # incremental backup of compressed files -> no good
  };

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?
}

