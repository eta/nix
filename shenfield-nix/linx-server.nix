{ pkgs, stdenv, ... }:

let
  sources = import ../npins/default.nix;
  rice = pkgs.buildGoModule {
    name = "rice";
    src = sources.go-rice.outPath;
    vendorHash = "sha256-KTT5Ld0Uyyfkhk29KuQuZoGG8UTz1E5Q7fUoSy7iKxM=";
  };
in
pkgs.buildGoModule {
    pname = "linx-server";
    version = "2.3.8";

    preInstallPhases = ["riceEmbedPhase"];

    src = sources.linx-server.outPath;

    riceEmbedPhase = ''
        ${rice}/bin/rice append -v --exec "$GOPATH/bin/linx-server"
        ${rice}/bin/rice append -v --exec "$GOPATH/bin/linx-genkey"
        ${rice}/bin/rice append -v --exec "$GOPATH/bin/linx-cleanup"
    '';

    dontStrip = true; # needed so the embedded assets don't get removed again

    vendorHash = "sha256-/N3AXrPyENp3li4X86LNXsfBYbjJulk+0EAyogPNIpc=";
}
