{ pkgs, lib, stdenv, rustPlatform, ... }:

let
  sources = import ../npins/default.nix;
in
rustPlatform.buildRustPackage {
  name = "doorbell";
  src = sources.doorbell.outPath;
  cargoHash = "sha256-v8Ae3kZuv3i2BSSYwoTUhcMbjxCfcr115Z1mxk2j6Y4";
  doCheck = false;
}
