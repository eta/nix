{
  log-level = "info";
  log-client-ip = true;
  application-name = "gotosocial";
  landing-page-user = "eta";
  host = "gotosocial.i.eta.st";
  account-domain = "eta.st";
  protocol = "https";
  bind-address = "[::]";
  port = 443;

  log-timestamp-format = "";

  letsencrypt-enabled = true;
  letsencrypt-cert-dir = "/var/lib/gotosocial/certs";
  
  db-type = "sqlite";
  db-address = "/var/lib/gotosocial/database.sqlite3";
  db-sqlite-journal-mode = "WAL";
  db-sqlite-synchronous = "NORMAL";
  db-sqlite-busy-timeout = "5m";

  accounts-registration-open = false;
  accounts-allow-custom-css = true;
  media-description-max-chars = 5000;
  media-remote-cache-days = 7;
  media-emoji-remote-max-size = 409600;
  
  storage-backend = "local";
  storage-local-base-path = "/var/lib/gotosocial/storage";

  oidc-enabled = true;
  oidc-idp-name = "Keycloak";
  oidc-issuer = "https://keycloak.i.eta.st/realms/etainfra";
  oidc-client-id = "gotosocial";
  # oidc-client-secret: provided via EnvironmentFile
  oidc-admin-groups = ["gotosocial-admin"];

  advanced-rate-limit-requests = 0;
  http-client = {
    allow-ips = ["64:ff9b::/96"];
    timeout = "90s";
  };
}
  

