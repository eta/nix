{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
  sources = import ../nix/sources.nix; 
in
{
  services.clickhouse.enable = true;

  fileSystems."/clickhouse" = {
    device = "/dev/disk/by-uuid/25315a6c-8286-4a97-a64d-878fcb396512";
    fsType = "ext4";
  };

  environment.etc."clickhouse-server/config.xml".source = lib.mkForce ./clickhouse/config.xml;
  environment.etc."clickhouse-server/users.xml".source = lib.mkForce ./clickhouse/users.xml;

  # I really don't want my clickhouse to ever get exposed to the internet, thanks
  systemd.services.clickhouse.requires = lib.mkAfter ["clickhouse-nftables.service"];
  # it'll get OOM killed a lot
  systemd.services.clickhouse.serviceConfig.Restart = "always";

  systemd.services.clickhouse-nftables = let
    nftables-conf = pkgs.writeTextDir "nftables.conf" ''
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
        chain input {
                type filter hook input priority 0;
                ct state vmap { invalid : drop, established : accept, related : accept }
                iif "lo" accept
                ip protocol icmp accept
                ip6 nexthdr ipv6-icmp accept
                ip saddr ${ipam.ipv4.intertube}/32 counter accept comment "intertube"
                ip6 saddr ${ipam.ipv6.intertube}/128 counter accept comment "intertube"
                reject;
        }
        chain forward {
                type filter hook forward priority 0;
        }
        chain output {
                type filter hook output priority 0;
        }
}
    '';
  in {
    description = "clickhouse nftables config";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.nftables}/bin/nft -f ${nftables-conf}/nftables.conf";
      RemainAfterExit = true;
      Type = "oneshot";
    };
  };

  networking.namespaces.clickhouse = {
    ipv4 = ipam.ipv4.clickhouse;
    ipv6 = ipam.ipv6.clickhouse;
    services = ["clickhouse" "clickhouse-nftables"];
  };
}
