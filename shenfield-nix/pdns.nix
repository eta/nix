{ config, lib, ... }:

let
  ipam = import ../ipam.nix;
in
{
  services.powerdns.enable = true;
  services.powerdns.extraConfig = ''
launch=gsqlite3
gsqlite3-database=/var/lib/powerdns/pdns.sqlite3
  '';

  networking.namespaces.pdns = {
    ipv4 = ipam.ipv4.pdns;
    ipv6 = ipam.ipv6.pdns;
    services = ["pdns"];
  };
}

