{ sslCrt, sslKey }:

{
    allow-environment-overrides = true;
    network = {
        name = "pandemonium";
    };
    server = {
        name = "pandemonium.i.eta.st";
        listeners = {
            "127.0.0.1:6667" = {};
            "[::1]:6667" = {};

            ":6697" = {
                tls = {
                    cert = sslCrt;
                    key = sslKey;
                };
                proxy = false;
                min-tls-version = 1.2;
            };
        };
        sts = {
            enabled = true;
            duration = "1mo2d5m";
            port = 6697;
        };
        casemapping = "precis";
        enforce-utf8 = true;
        lookup-hostnames = true;
        forward-confirm-hostnames = true;
        check-ident = true;
        # set by EnvironmentFile. we wouldn't people reading this out, now would we?
        password = "lalala I get replaced by an env var hopefully";
        motd = "/var/lib/ergo/ergo.motd";
        motd-formatting = true;
        relaymsg = {
            enabled = true;
            separators = "/";
            available-to-chanops = true;
        };
        max-sendq = "96k";
        ip-cloaking.enabled = false;
        override-services-hostname = "pandemonium.i.eta.st";
    };
    accounts = {
        authentication-enabled = true;
        registration = {
            enabled = true;
            allow-before-connect = false;
            bcrypt-cost = 4;
            verify-timeout = "32h";
        };
        skip-server-password = true;
        login-via-pass-command = true;
        nick-reservation = {
            enabled = true;
            additional-nick-limit = 0;
            method = "optional";
            allow-custom-enforcement = true;
            force-nick-equals-account = false;
        };
        multiclient = {
            enabled = true;
            allowed-by-default = true;
            always-on = "opt-in";
            auto-away = "opt-out";
            always-on-expiration = "7d";
        };
    };
    channels = {
        default-modes = "+n";
        max-channels-per-client = 100;
        operator-only-creation = false;
        registration = {
            enabled = true;
            operator-only = false;
        };
    };
    logging = [{
        method = "stderr";
        type = "* -userinput -useroutput";
        level = "debug";
    }];
    #opers.admin.password = "I should also be replaced with an env var, please";
    datastore = {
        path = "/var/lib/ergo/ircd.db";
        autoupgrade = true;
    };
    history = {
        enabled = true;
        channel-length = 2048;
        client-length = 256;
        autoreplay-on-join = 0;
    };
    limits = {
        nicklen = 32;
        identlen = 20;
        channellen = 64;
        awaylen = 390;
        kicklen = 390;
        topiclen = 390;
    };
}