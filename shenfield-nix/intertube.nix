{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
  sources = import ../npins/default.nix;
  intertube = (import sources.intertube.outPath { inherit pkgs; }).override {
    codeVersion = builtins.substring 0 7 sources.intertube.revision;
  };
  intertube-redis-fwd = pkgs.writeShellScriptBin "intertube-redis-fwd" ''
    echo "will forward localhost:6379 to intertube's redis..."
    sudo ${pkgs.socat}/bin/socat tcp-listen:6379,fork,reuseaddr,bind=127.0.0.1 exec:'ip netns exec intertube ${pkgs.socat}/bin/socat STDIO "tcp-connect:127.0.0.1:6379"',nofork
  '';
in
{
  environment.systemPackages = lib.mkAfter [ intertube-redis-fwd ];

  services.redis.servers.intertube = {
    enable = true;
    port = 6379;
  };

  services.redis.vmOverCommit = true;

  services.multicaddy.intertube = {
    config = ''
    intertube.eta.st

    reverse_proxy localhost:4000
    '';
  };

  # FIXME(eta): investigate the weird
  #      Name service error in "getaddrinfo": -11 (System error)
  # that keeps causing outages. For now, hackily patch it up...

  networking.extraHosts = lib.mkAfter ''
    13.74.170.167 cloud.tfl.gov.uk
  '';

  systemd.services.intertube-scraper = {
    description = "intertube scraper backend";
    wantedBy = [ "multi-user.target" ];
    bindsTo = [ "redis-intertube.service" ];
    after = [ "redis-intertube.service" ];

    serviceConfig = {
      ExecStart = "${intertube}/bin/intertube-scraper --dynamic-space-size 6144 --disable-ldb";
      DynamicUser = true;
      RuntimeDirectory = "intertube-scraper";
      Restart = "on-failure";
      RestartSec = 3;
      EnvironmentFile = "/var/lib/intertube-env";
    };
  };

  systemd.services.intertube-web = {
    description = "intertube website";
    wantedBy = [ "multi-user.target" ];
    bindsTo = [ "redis-intertube.service" ];
    after = [ "redis-intertube.service" ];

    serviceConfig = {
      ExecStart = "${intertube}/bin/intertube-web --disable-ldb";
      DynamicUser = true;
      RuntimeDirectory = "intertube-web";
      Restart = "on-failure";
      RestartSec = 3;
    };
  };

  networking.namespaces.intertube = {
    ipv4 = ipam.ipv4.intertube;
    ipv6 = ipam.ipv6.intertube;
    services = ["intertube-scraper" "intertube-web" "redis-intertube" "caddy-intertube"];
  };

}
