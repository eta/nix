{
  ipv4 = {
    shnix0-shenfield = "44.31.189.25";
    shenfield-nix = "44.31.189.26";
    holborn = "44.31.189.2";
    hammersmith = "44.31.189.3";

    gotosocial = "44.31.189.154";
    step = "44.31.189.145";
    state-reset = "44.31.189.152";
    ejabberd = "44.31.189.141";
    pandemonium = "44.31.189.133";
    keycloak = "44.31.189.139";
    intertube = "44.31.189.124";
    gitlab = "44.31.189.150";
    eta-site-a = "44.31.189.135";
    honk = "44.31.189.136";
    doorbell = "44.31.189.122";
    wiki = "44.31.189.63";
    emf-gsm-testing = "44.31.189.61";
    clickhouse = "44.31.189.60";
    pdns = "44.31.189.58";
    outertube = "44.31.189.57";

    # /29 for vpn clients
    vpn-network = "44.31.189.32";
    vpn-server = "44.31.189.33";
    vpn-euston = "44.31.189.34";
    vpn-der-gerat = "44.31.189.35";
    vpn-brd = "44.31.189.38";

    home-network-hammersmith-native = "10.99.0.1";
    home-network-hammersmith-tayga = "10.99.64.1";
  };
  ipv6 = {
    shenfield-nix = "2a0d:1a40:7553:fefe:5054:ff:fedf:a136";
    shenfield-lxc = "2a0d:1a40:7553:e0ff:5054:ff:fe4d:10a7";
    holborn = "2a0d:1a40:7553::1";
    hammersmith = "2a0d:1a40:7553::2";
    shenfield = "2a0d:1a40:7553::3";

    gotosocial = "2a0d:1a40:7553:10::1";
    step = "2a0d:1a40:7553:10::2";
    state-reset = "2a0d:1a40:7553:10::3";
    ejabberd = "2a0d:1a40:7553:10::4";
    pandemonium = "2a0d:1a40:7553:feed:216:abff:fea1:7adf";
    keycloak = "2a0d:1a40:7553:10::5";
    intertube = "2a0d:1a40:7553:10::6";
    gitlab = "2a0d:1a40:7553:10::7";
    eta-site-a = "2a0d:1a40:7553:feed:216:abff:fefb:72ac";
    honk = "2a0d:1a40:7553:feed:216:abff:fea9:a15b";
    doorbell = "2a0d:1a40:7553:10::8";
    wiki = "2a0d:1a40:7553:10::9";
    emf-gsm-testing = "2a0d:1a40:7553:10::11";
    clickhouse = "2a0d:1a40:7553:10::12";
    pdns = "2a0d:1a40:7553:10::13";
    outertube = "2a0d:1a40:7553:10::14";

    vpn-server = "2a0d:1a40:7553:42::1";
    vpn-euston = "2a0d:1a40:7553:42::2";
    vpn-der-gerat = "2a0d:1a40:7553:42::3";

    home-network-hammersmith = "2a0d:1a40:7553:20::1"; # /64
  };
}
