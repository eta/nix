# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
in
{
  imports =
    [
      ../shared/generic-server.nix
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda";

  networking.hostName = "holborn";
  systemd.network.networks."10-uplink" = {
    matchConfig = {
      Name = "ens3";
    };

    networkConfig = {
      DHCP = "ipv4";
      Address = "2a00:1098:84:23b::1/48";
    };
  };
  systemd.network.netdevs."dummy0" = {
    netdevConfig = {
      Name = "dummy0";
      Kind = "dummy";
    };
  };
  systemd.network.networks."dummy0" = {
    matchConfig = {
      Name = "dummy0";
    };

    networkConfig = {
      Address = [ "${ipam.ipv4.holborn}/32" "${ipam.ipv6.holborn}/128" ];
    };
  };

  services.bird2 = {
    enable = true;
    config = import ../shared/bird.conf.nix {
      ipv4 = ipam.ipv4.holborn;
      ipv6 = ipam.ipv6.holborn;
      ospfInterfaces = ["shenfield" "hammersmith"];
      extraConfig = builtins.readFile ./extra-bird.conf;
    };
  };

  networking.wireguard.interfaces = {
    shenfield = {
      ips = [ "10.42.4.0/31" "fe80:d::a/127" ];
      listenPort = 51823;
      privateKeyFile = "/var/lib/wgkey-shenfield";
      allowedIPsAsRoutes = false; # !!!

      peers = [{
        publicKey = "YCWVK0tH3SQUqANtEiNTCinlvR6Y15bBGIucQzZ+dwc=";
        allowedIPs = [ "0.0.0.0/0" "::/0" ];
      }];
    };
    eva = {
      ips = [ "172.16.16.2/24" "fe80:eeee::a/64" ];
      listenPort = 11605;
      privateKeyFile = "/var/lib/wgkey-eva";
      allowedIPsAsRoutes = false;

      peers = [{
        publicKey = "jq9Ofggqfc1HkmvnLJfVZp7z6ID67aQaVyL2uXT8LDE=";
        allowedIPs = [ "0.0.0.0/0" "::/0" ];
        endpoint = "46.227.205.145:11605";
      }];
    };
    hammersmith = {
      ips = [ "10.43.0.0/31" "fe80:a::a/127" ];
      listenPort = 51820;
      privateKeyFile = "/var/lib/wgkey-hammersmith";
      allowedIPsAsRoutes = false;

      peers = [{
        publicKey = "jqIuqAUwPW6M81+4S1FdXkpfr8Re03pWillq+AtH5GU=";
        allowedIPs = [ "0.0.0.0/0" "::/0" ];
      }];
    };
  };

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

