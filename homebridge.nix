{ lib
, buildNpmPackage
, fetchFromGitHub
, nodejs_20
}:

buildNpmPackage rec {
  pname = "homebridge";
  version = "1.8.4";
  src = fetchFromGitHub {
    owner = "homebridge";
    repo = "homebridge";
    rev = "v${version}";
    hash = "sha256-92mfisXSrl8D7taJWgZ+iO6EprJPmINnQgtOQ72YYPI=";
  };

  nodejs = nodejs_20;

  buildPhase = "npx tsc";

  npmDepsHash = "sha256-f9L80jbW+VB3yXwq1WrRJooCuQpTOW79pkRPSdHURTk=";

  meta = {
    description = "Homebridge";
    homepage = "https://github.com/homebridge/homebridge";
    license = lib.licenses.asl20;
    mainProgram = "homebridge";
  };
}
