# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

let
  ipam = import ../ipam.nix;
  sources = import ../npins/default.nix;
  erbium = import sources.erbium.outPath { nixpkgs = pkgs; };
  homebridge = pkgs.callPackage ../homebridge.nix {};
  homebridge-z2m = pkgs.callPackage ../homebridge-z2m.nix {};
in
{
  imports =
    [ # Include the results of the hardware scan.
      ../shared/generic-server.nix
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "hammersmith";

  systemd.network.netdevs."ont0" = {
    netdevConfig = {
      Name = "ont0";
      Kind = "vlan";
    };
    vlanConfig = {
      Id = 10;
    };
  };

  systemd.network.networks."ont0" = {
    matchConfig = {
      Name = "ont0";
    };
    networkConfig = {
      LinkLocalAddressing = "no";
    };
    linkConfig = {
      MTUBytes = "1508"; # for baby jumbo frames
      RequiredForOnline = "carrier";
    };
  };

  services.pppd = {
    enable = true;
    peers.idnet = {
      autostart = true;
      enable = true;
      config = ''
        plugin pppoe.so
        noipdefault
        defaultroute
        replacedefaultroute
        persist
        maxfail 0
        holdoff 5
        nic-ont0
        user "FTTP.207182@idnet"
        mtu 1500
        lcp-echo-interval 1
        lcp-echo-failure 10
      '';
    };
  };

  systemd.network.netdevs."dummy0" = {
    netdevConfig = {
      Name = "dummy0";
      Kind = "dummy";
    };
  };

  systemd.network.networks."dummy0" = {
    matchConfig = {
      Name = "dummy0";
    };

    networkConfig = {
      Address = [ "${ipam.ipv4.hammersmith}/32" "${ipam.ipv6.hammersmith}/128" ];
    };
    routingPolicyRules = [
    {
      routingPolicyRuleConfig = {
        From = "44.31.189.0/24";
        Table = "44";
      };
    }
    {
      routingPolicyRuleConfig = {
        To = "44.31.189.0/24";
        Table = "44";
      };
    }
    {
      routingPolicyRuleConfig = {
        From = "2a0d:1a40:7553::/48";
        Table = "44";
      };
    }
    {
      routingPolicyRuleConfig = {
        To = "2a0d:1a40:7553::/48";
        Table = "44";
      };
    }
    {
      routingPolicyRuleConfig = {
        From = "10.99.0.0/16";
        Table = "44";
      };
    }
    {
      routingPolicyRuleConfig = {
        To = "10.99.0.0/16";
        Table = "44";
      };
    }
    # Make VoWiFi ePDG endpoints go out the normal internet (for geolocation reasons):
    #
    # eta@hammersmith ~> dig +short epdg.epc.mnc033.mcc234.pub.3gppnetwork.org
    # 46.68.57.32
    # 46.68.58.10
    # 46.68.59.19
    {
      routingPolicyRuleConfig = {
        To = "46.68.59.19/32";
        Table = "main"; 
        Priority = 100; # apply before the other rules
      };
    }
    {
      routingPolicyRuleConfig = {
        To = "46.68.57.32/32";
        Table = "main"; 
        Priority = 101;
      };
    }
    {
      routingPolicyRuleConfig = {
        To = "46.68.58.10/32";
        Table = "main"; 
        Priority = 102;
      };
    }
    ];
  };

  services.bird2 = {
    enable = true;
    config = import ../shared/bird.conf.nix {
      ipv4 = ipam.ipv4.hammersmith;
      ipv6 = ipam.ipv6.hammersmith;
      ospfInterfaces = ["holborn" "shenfield"];
      kernelTable = "44";
      extraConfig = builtins.readFile ./extra-bird.conf;
    };
  };

  networking.wireguard.interfaces = {
    holborn = {
      listenPort = 51820;
      privateKeyFile = "/var/lib/wgkey-holborn";
      allowedIPsAsRoutes = false;

      peers = [{
        publicKey = "CNf2IsaR7KiNW2Dzg7fpQaHhgTLTy/9rdZevXl0OGz0=";
        allowedIPs = [ "0.0.0.0/0" "::/0" ];
        persistentKeepalive = 25;
        endpoint = "176.126.240.34:51820";
      }];
    };
    shenfield = {
      listenPort = 51821;
      privateKeyFile = "/var/lib/wgkey-shenfield";
      allowedIPsAsRoutes = false;

      peers = [{
        publicKey = "ftkA0Jy1xo1vpe1cM849U+Qb3RtBtkJcyhF2uRhjqQs=";
        allowedIPs = [ "0.0.0.0/0" "::/0" ];
        persistentKeepalive = 25;
        endpoint = "93.93.131.17:51821";
      }];
    };
  };

  systemd.network.networks."10-lan" = {
    matchConfig = {
      Name = "eno1";
    };
    networkConfig = {
      Address = [ "${ipam.ipv4.home-network-hammersmith-native}/24" "${ipam.ipv6.home-network-hammersmith}/64" ];
      IPv6AcceptRA = "no";
      VLAN = "ont0";
    };
    cakeConfig = {
      Bandwidth = "900M";
      NAT = "yes";
    };
    linkConfig = {
      MTUBytes = "1508"; # for baby jumbo frames on ont0
    };
  };

  systemd.network.networks."20-shenfield" = {
    matchConfig = {
      Name = "shenfield";
    };
    networkConfig = {
      Address = [ "10.42.5.1/31" "fe80:e::b/127" ];
    };
    cakeConfig = {
      Bandwidth = "90M";
      NAT = "yes";
    };
  };

  systemd.network.networks."20-holborn" = {
    matchConfig = {
      Name = "holborn";
    };
    networkConfig = {
      Address = [ "10.43.0.1/31" "fe80:a::b/127" ];
      IPv6AcceptRA = "no";
    };
    cakeConfig = {
      Bandwidth = "90M";
      NAT = "yes";
    };
  };

  # FIXME(eta): this also needs ipam-ing
  systemd.services.erbium-dhcp = let
    erbiumConfig = pkgs.writeTextDir "erbium.conf" ''
    addresses: [10.99.0.0/24, 2a0d:1a40:7553:20::/64] 
    dns-servers: [1.1.1.1]
    dns-search: [i.eta.st]
    api-listeners: ["[::1]:9968", "/var/lib/erbium/control"]
    dns-listeners: ["[::1]:4555"]
    dhcp-policies:
      - match-subnet: 10.99.0.0/24
        apply-routers: [10.99.0.1]
    router-advertisements:
      eno1:
        lifetime: 1h
        mtu: 1400
        hop-limit: 64
        prefixes:
        - prefix: 2a0d:1a40:7553:20::/64
          on-link: true
          autonomous: true
          valid: 30d
          preferred: 7d
    '';
  in {
    description = "Erbium DHCP server";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      DynamicUser = true;
      StateDirectory = "erbium";
      AmbientCapabilities = "CAP_NET_RAW CAP_NET_BIND_SERVICE";
      ExecStart = "${erbium}/bin/erbium ${erbiumConfig}/erbium.conf";
    };
  };

  systemd.services.homebridge = {
    description = "homebridge";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      DynamicUser = true;
      StateDirectory = "homebridge";
      ExecStart = "${homebridge}/bin/homebridge --strict-plugin-resolution -U /var/lib/homebridge/ -P ${homebridge-z2m}/lib/node_modules";
    };
  };

  systemd.services.isponsorblocktv = {
    description = "isponsorblocktv";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      DynamicUser = true;
      StateDirectory = "isponsorblocktv";
      ExecStart = "${pkgs.isponsorblocktv}/bin/iSponsorBlockTV -d /var/lib/isponsorblocktv/ start";
      Restart = "always";
    };
  };

  systemd.services.nftables = let
    nftables-conf = pkgs.writeTextDir "nftables.conf" ''
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
        chain input {
                type filter hook input priority 0;
                ct state vmap { invalid : drop, established : accept, related : accept }
                iif "lo" accept
                ip protocol icmp accept
                ip protocol ospfigp accept
                ip6 nexthdr ospfigp accept
                ip6 nexthdr ipv6-icmp accept
                udp dport 3784 accept comment "bfd"

                tcp dport 22 accept comment "ssh access"
                iifname eno1 accept comment "LAN access"
                ip6 saddr ${ipam.ipv6.shenfield-lxc}/128 tcp dport 8000 accept comment "shenfield-lxc backups"
                ip6 saddr ${ipam.ipv6.shenfield-nix}/128 tcp dport 8000 accept comment "shenfield-nix backups"
                ip6 saddr ${ipam.ipv6.shenfield}/128 tcp dport 8000 accept comment "shenfield backups"
                reject
        }
        chain forward {
                type filter hook forward priority 0;
                # MSS clamping
                tcp flags syn tcp option maxseg size set rt mtu

                ct state vmap { invalid : drop, established : accept, related : accept }
                iifname eno1 accept

                reject
        }
        chain output {
                type filter hook output priority 0;
        }
        chain postrouting {
                type nat hook postrouting priority 100; policy accept;

                # masquerade the internal LAN
                ip saddr 10.99.0.0/16 oifname shenfield snat to ${ipam.ipv4.hammersmith}
                ip saddr 10.99.0.0/16 oifname holborn snat to ${ipam.ipv4.hammersmith}
                # masquerade when going out idnet, too
                ip saddr 10.99.0.0/16 oifname ppp0 masquerade
        }
}
    '';
  in {
    description = "nftables config";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.nftables}/bin/nft -f ${nftables-conf}/nftables.conf";
      RemainAfterExit = true;
      Type = "oneshot";
    };
  };

  # this bit me while plugging something in!
  services.logind.powerKey = "ignore";
  services.logind.powerKeyLongPress = "poweroff";

  services.mosquitto = {
    enable = true;
    listeners = [
      {
        acl = [ "pattern readwrite #" ];
        omitPasswordAuth = true;
        settings.allow_anonymous = true;
      }
    ];
  };
  
  services.zigbee2mqtt = {
    enable = true;
    settings = {
      mqtt.server = "mqtt://localhost:1883/";
      serial.port = "/dev/ttyUSB0";
      frontend.port = 8099; # Needed to avoid conflict with unifi
    };
  };

  services.unifi = {
    enable = true;
    unifiPackage = pkgs.unifi8;
    mongodbPackage = pkgs.callPackage ../mongodb-ce.nix {};
  };

  services.collectd = {
    enable = true;
    buildMinimalPackage = true;
    plugins = {
      cpu = "";
      cpufreq = "";
      df = "";
      disk = "";
      interface = "";
      load = "";
      memory = "";
      network = "Server \"metrics.i.eta.st\"";
      processes = "";
      sensors = "";
      swap = "";
      thermal = "";
      users = "";
    };
  };

  boot.supportedFilesystems = [ "zfs" ];
  networking.hostId = "0dd0203c";
  boot.zfs.forceImportRoot = false;

  users.users.rest-server = {
    isSystemUser = true;
    group = "rest-server";
  };
  users.groups.rest-server = {};

  systemd.services.rest-server = {
    description = "rest-server restic backup target";
    wantedBy = [ "multi-user.target" ];
    unitConfig = {
      RequiresMountsFor = "/backups";
    };
    serviceConfig = {
      User = "rest-server";
      Group = "rest-server";
      ExecStart = "${pkgs.restic-rest-server}/bin/rest-server --prometheus --prometheus-no-auth --path /backups/restic/ --append-only";
      Restart = "always";
    };
  };

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.05"; # Did you read the comment?

}

