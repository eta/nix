{ lib
, SDL2
, cmake
, extra-cmake-modules
, fetchFromGitHub
, libarchive
, libpcap
, qt6
, libslirp
, libGL
, pkg-config
, stdenv
, wayland
, zstd
}:

stdenv.mkDerivation (finalAttrs: {
  pname = "melonDS";
  version = "0.9.5-unstable-2024-03-10";

  src = fetchFromGitHub {
    owner = "melonDS-emu";
    repo = "melonDS";
    rev = "b117bb8f58938edf3da1e348ca5ff32fa68b279e";
    hash = "sha256-Y9JyqaaTkae1jB91grMjnw9hptd0h8scr7J9nAsfwaI=";
  };

  patches = [
    ./miyuko-rhythm-heaven.patch
  ];

  nativeBuildInputs = [
    cmake
    pkg-config
    qt6.wrapQtAppsHook
  ];

  buildInputs = [
    cmake
    SDL2
    extra-cmake-modules
    libarchive
    libslirp
    libGL
    qt6.qtbase
    qt6.qtmultimedia
    zstd
  ] ++ (if stdenv.isLinux then [ wayland ] else []);

  cmakeFlags = [ "-DUSE_QT6=ON" ];

  installPhase = ''
    mkdir -p $out/
  '' + (if stdenv.isLinux then ''
    cp -r melonDS $out/
  '' else ''
    cp -r melonDS.app/ $out/
  '');

  strictDeps = true;

  qtWrapperArgs = [
    "--prefix LD_LIBRARY_PATH : ${lib.makeLibraryPath [ libpcap ]}"
  ];

  meta = {
    homepage = "https://melonds.kuribo64.net/";
    description = "Work in progress Nintendo DS emulator";
    license = with lib.licenses; [ gpl3Plus ];
    mainProgram = "melonDS";
    maintainers = with lib.maintainers; [
      AndersonTorres
      artemist
      benley
      shamilton
      xfix
    ];
  };
})
