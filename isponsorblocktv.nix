{ lib
, python3Packages
, fetchFromGitHub
, pkgs
}:

let
  customPackages = import ./isponsorblocktv-deps.nix { inherit pkgs; };
in
python3Packages.buildPythonApplication rec {
  pname = "iSponsorBlockTV";
  version = "2.2.1";
  pyproject = true;

  src = fetchFromGitHub {
    owner = "dmunozv04";
    repo = pname;
    rev = "v" + version;
    hash = "sha256-v5NF6o+9IxusYNebs2a9fgHCHZNN9hHLQurujhmdsgU=";
  };

  build-system = with python3Packages; [
    hatchling
    hatch-requirements-txt
  ];

  dependencies = with customPackages; [
    python3Packages.aiohttp
    appdirs
    argparse
    async-cache
    pyytlounge
    rich
    ssdp
    textual
    textual-slider
    xmltodict
    rich-click
  ];
}
